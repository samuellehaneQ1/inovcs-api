# Drawing Images and Videos and Manipulating Their Position

In the following example we will draw a "named" asset on a display and manipulate it’s position.

In the example below we will use device of id 0

```json
{ "id": "0" }
```

## Overview
1. Draw the Video/Picture Element
2. Manipulate the Asset using the reload Command
3. Benefit of using the base constructor to reload the elements

## 1. Draw the Image/Video Asset
After having created a connection and authenticated to the server we will show an image asset. When we show the image asset we will give it a name so that we can reference it later.

#### Drawing "image1.jpg" with the name "asset1" (json Object)

```json
{
    "type": "device",
    "line": "showImage",
    "device": {
        "id": "0"
    },
    "args": { 
        "source": "/image1.jpg",    // Image url
        "name": "asset1"            // The string name to reference the asset
    },
    // Session Token,
    // API
}
```
## 2. Manipulate the Asset Using The Reload Command
We can now reload the asset to manipulate properties. There are a range of different keyword arguments we can use to manipulate our assets.

```json
{ "line"	: "reload" }
```

#### Reloading The Position And Size Of A Named Asset

```json
{
    "type"	: "device",
    "line"	: "reload",
    "device"	: {
        "id" 	: "0"
    },
    "args"	: {
        "name"	: "asset1",  // The string name to reference the asset

        // All Arguments below are optional

        // Position & Size
        "pos_hint": {                   // X&Y by default are from the bottom left corner.
            "x": 0,                     // X position value of the asset from 0-1
            "y": 0                      // Y position value of the asset from 0-1
        },
        "size_hint": [1,1],             // The float X Y size of the asset from 0-1.

        "bringToFront": true,           // Bring the asset to the front of the asset stack
        "volume": 1,                    // Volume float 0-1. For video/audio assets.

        // Background Color.
        // When an image has transparency or is a different aspect to the surrounding box.
        "backgroundColor": [0,0,0,1],    // RGB or RGBA Array. Float from 0-1.

        // Animations
        "animation": "fadeIn",              // Animation to use when redrawing (currently only supports 'fadeIn')
        "animationDuration": 1,             // Animation Durations in Seconds (float)

        // Element can also be set a duration to remove it.
        "duration": 1,                      // Durations in Seconds (float)

        // Animation Out. Only Applicable when duration has been used.
        "animationOut": "fadeOut",          // Animation when duration is complete (currently only supports 'fadeOut')
        "animationOutDuration": 1           // Animation Durations in Seconds (float)

    }
    // Session Token,
    // API
}
```
In the above example we have referenced our asset as “asset1” this string can be any unique name for the asset.

### Example Reload Commands
Run the Following Commands in the device controller to see a working example

#### Draw the Video. Here we will use the Google Test Video.

```json
{
    "type": "device",
    "line": "showLocalVideo",
    "device": {
        "id": "0"
    },
    "args": {
        "source": "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
        "name": "asset1",
        "pos_hint": {
            "x": 0,
            "y": 0
        },
        "size_hint": [1,1]
    }
}
```

#### Make the Video of Height 0.5 and Width 0.5. Set the Position to X: 0.25 and Y: 0.25

```json
{
    "type": "device",
    "line": "reload",
    "device": {
        "id": "0"
    },
    "args": {
        "name": "asset1",
        "pos_hint": {
            "x": 0.25,
            "y": 0.25
        },
        "size_hint": [0.5,0.5]
    }
}
```

#### Add an animation to the reload

```json
{
    "type": "device",
    "line": "reload",
    "device": {
        "id": "0"
    },
    "args"	: {
        "name": "asset1",
        "pos_hint": {
            "x": 0.75,
            "y": 0.75
        },
        "size_hint": [0.25,0.25],
        "animation": "fadeIn",
        "animationOutDuration": 1
    }
}
```

From the example this should give you a brief introduction to manipulating elements on the displays canvas.


## 3. Benefits Of Using The Base Constructor To Reload the Elements

When modifying an element it is possible to use the base constructor to reload the asset. The benefit of doing this is that if something has happened to the display and it has been reset from a power outage or other factors, The display will know to draw the asset if it does not exist and what arguments should currently be present.

#### Reloading the the base constructor takes the exact same arguments as a reload command

### Example Reload Commands using the base constructor
Run the Following Commands in the device controller to see a working example

#### Start by blacking out the canvas

```json
{
    "type": "device",
    "line": "blackout",
    "device": {
        "id": "0"
    },
}
```

#### Draw the Asset. Here we will use the Google Test Video. Notice we are now using animations on all of our commands. We have also told the video to loop.

```json
{
    "type": "device",
    "line": "showLocalVideo",
    "device": {
        "id": "0"
    },
    "args": {
        "source": "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
        "name": "asset1",
        "pos_hint": {
            "x": 0,
            "y": 0
        },
        "size_hint": [1,1],
        "animation": "fadeIn",
        "animationDuration": 1,
        "state": "play",
        "loop": true
    }
}
```

#### Make the Video of Height 0.5 and Width 0.5. Set the Position to X: 0.25 and Y: 0.25

```json
{
    "type": "device",
    "line": "showLocalVideo",
    "device": {
        "id": "0"
    },
    "args": {
        "name": "asset1",
        "pos_hint": {
            "x": 0.25,
            "y": 0.25
        },
        "size_hint": [0.5,0.5],
        "animation": "fadeIn",
        "animationOutDuration": 1
    }
}
```

#### Move the Video to the to Right Corner

```json
{
    "type": "device",
    "line": "showLocalVideo",
    "device": {
        "id": "0"
    },
    "args"	: {
        "name": "asset1",
        "pos_hint": {
            "x": 0.75,
            "y": 0.75
        },
        "size_hint": [0.25,0.25],
        "animation": "fadeIn",
        "animationOutDuration": 1
    }
}
```

#### Move the Video back to Full Screen

```json
{
    "type": "device",
    "line": "showLocalVideo",
    "device": {
        "id": "0"
    },
    "args"	: {
        "name": "asset1",
        "pos_hint": {
            "x": 0,
            "y": 0
        },
        "size_hint": [1,1],
        "animation": "fadeIn",
        "animationOutDuration": 1
    }
}
```

## Video Player State.

Now Run the following commands to preview the state control.

#### Pause the Video

```json
{
    "type": "device",
    "line": "showLocalVideo",
    "device": {
        "id": "0"
    },
    "args"	: {
        "name": "asset1",
        "state": "pause"
    }
}
```

#### Play the Video

```json
{
    "type": "device",
    "line": "showLocalVideo",
    "device": {
        "id": "0"
    },
    "args"	: {
        "name": "asset1",
        "state": "play"
    }
}
```

#### Stop the Video. The Video will hold the last frame.

```json
{
    "type": "device",
    "line": "showLocalVideo",
    "device": {
        "id": "0"
    },
    "args"	: {
        "name": "asset1",
        "state": "stop"
    }
}
```

#### Call display blackout to clear our display. This call can be used to stop if we also need to make the screen black

```json
{
    "type": "device",
    "line": "blackout",
    "device": {
        "id": "0"
    },
}
```